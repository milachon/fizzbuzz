package com.milachon.fizzbuzz.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.milachon.fizzbuzz.domain.exception.FizzBuzzException;
import com.milachon.fizzbuzz.domain.model.FizzBuzzRequest;
import com.milachon.fizzbuzz.manager.FizzBuzzRequestManager;

@Service
public class FizzBuzzService {

  @Autowired FizzBuzzRequestManager reqManager;

  public List<String> fizzBuzz(Integer nb1, Integer nb2, Integer limit, String str1, String str2)
      throws FizzBuzzException {

    FizzBuzzRequest req = new FizzBuzzRequest();
    req.setNb1(nb1);
    req.setNb2(nb2);
    req.setLimit(limit);
    req.setStr1(str1);
    req.setStr2(str2);

    // check if already referenced request
    FizzBuzzRequest savedReq = reqManager.getCorrespondingRequest(req);
    if (savedReq != null) {
      req = savedReq;
    }

    try {
      // compute if necessary
      if (req.getResult() == null || req.getResult().isEmpty()) {
        req.setResult(computeFizzBuzzRequest(req));
      }
    } finally {
      // update DB for log, even if compute fail
      req.setCounter(req.getCounter() + 1);
      reqManager.save(req);
    }
    return req.getResult();
  }

  private List<String> computeFizzBuzzRequest(FizzBuzzRequest req) throws FizzBuzzException {

    List<String> result = new ArrayList<>();

    // input data validation
    if (req.getLimit() < 0) {
      throw new FizzBuzzException("Limit " + req.getLimit() + " is invalid, limit must be >=1");
    }
    if (req.getNb1() < 1 || req.getNb2() < 1) {
      throw new FizzBuzzException(
          "Multiple value param must be >=1, here " + req.getNb1() + " and " + req.getNb2());
    }

    // computation
    String str3 = req.getStr1() + req.getStr2();

    // TODO see for more optimize version using directly index
    for (int ii = 1; ii < req.getLimit() + 1; ii++) {

      String current;
      Integer mod1 = ii % req.getNb1();
      Integer mod2 = ii % req.getNb2();

      if (mod1 == 0 && mod2 == 0) {
        current = str3;
      } else if (mod1 == 0) {
        current = req.getStr1();
      } else if (mod2 == 0) {
        current = req.getStr2();
      } else {
        current = String.valueOf(ii);
      }

      result.add(current);
    }

    return result;
  }
}
