package com.milachon.fizzbuzz.controller;

import java.util.List;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.milachon.fizzbuzz.domain.exception.FizzBuzzException;
import com.milachon.fizzbuzz.domain.model.FizzBuzzRequest;
import com.milachon.fizzbuzz.manager.FizzBuzzRequestManager;
import com.milachon.fizzbuzz.service.FizzBuzzService;

@RestController
@RequestMapping("/fizzbuzz")
@Validated
public class FizzBuzzRestController {

  @Autowired protected FizzBuzzService service;
  @Autowired protected FizzBuzzRequestManager manager;

  @GetMapping(value = "/compute")
  public ResponseEntity<List<String>> computeFizzBuzz(
      @RequestParam @Min(1) int int1,
      @RequestParam @Min(1) int int2,
      @RequestParam @Min(1) int limit,
      @RequestParam String str1,
      @RequestParam String str2) {

    try {
      return new ResponseEntity<>(service.fizzBuzz(int1, int2, limit, str1, str2), HttpStatus.OK);
    } catch (FizzBuzzException e) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST, "Error during computing FizzBuzz", e);
    }
  }

  @GetMapping(value = "/most_called")
  public ResponseEntity<FizzBuzzRequest> getMostCalledRequest() {

    FizzBuzzRequest res = manager.getMostCalledRequest();

    return new ResponseEntity<>(res, res == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
  }
}
