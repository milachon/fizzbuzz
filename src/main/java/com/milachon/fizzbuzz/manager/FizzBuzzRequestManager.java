package com.milachon.fizzbuzz.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.milachon.fizzbuzz.domain.model.FizzBuzzRequest;
import com.milachon.fizzbuzz.repository.IFizzBuzzRequestRepository;

@Service
public class FizzBuzzRequestManager {

  @Autowired protected IFizzBuzzRequestRepository repo;

  public FizzBuzzRequest save(FizzBuzzRequest entity) {
    return repo.save(entity);
  }

  public FizzBuzzRequest getCorrespondingRequest(FizzBuzzRequest req) {
    return repo.findByNb1AndNb2AndLimitAndStr1AndStr2(
        req.getNb1(), req.getNb2(), req.getLimit(), req.getStr1(), req.getStr2());
  }

  public FizzBuzzRequest getMostCalledRequest() {
    return repo.findFirstByOrderByCounterDesc();
  }
}
