package com.milachon.fizzbuzz.domain.exception;

public class FizzBuzzException extends Exception {

  /** */
  private static final long serialVersionUID = 193297003260021451L;

  public FizzBuzzException(String errorMessage) {
    super(errorMessage);
  }
}
