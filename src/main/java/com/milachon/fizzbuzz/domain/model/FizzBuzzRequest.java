package com.milachon.fizzbuzz.domain.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "fizz_buzz_request")
@Getter
@Setter
@EqualsAndHashCode
public class FizzBuzzRequest {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(unique = true, nullable = false)
  private String uuid;

  @Column private Integer nb1;

  @Column private Integer nb2;

  @Column(name = "max") // limit is reserved by hibernate
  private Integer limit;

  @Column private String str1;

  @Column private String str2;

  @ElementCollection private List<String> result = null;

  @Column private Integer counter = 0;
}
