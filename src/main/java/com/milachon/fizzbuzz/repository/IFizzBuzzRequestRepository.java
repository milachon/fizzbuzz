package com.milachon.fizzbuzz.repository;

import org.springframework.data.repository.CrudRepository;
import com.milachon.fizzbuzz.domain.model.FizzBuzzRequest;

public interface IFizzBuzzRequestRepository extends CrudRepository<FizzBuzzRequest, String> {

  public FizzBuzzRequest findByNb1AndNb2AndLimitAndStr1AndStr2(
      Integer nb1, Integer nb2, Integer limit, String str1, String str2);

  public FizzBuzzRequest findFirstByOrderByCounterDesc();
}
