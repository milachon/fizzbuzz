package com.milachon.fizzbuzz.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.milachon.fizzbuzz.domain.exception.FizzBuzzException;

@SpringBootTest
public class FizzBuzzServiceTest {

  protected String uri = "/fizzbuzz";

  @Autowired protected FizzBuzzService service;

  @Test
  public void fizzbuzzTest() throws Exception {

    List<String> expected = new ArrayList<String>();
    expected.add("1");
    expected.add("toto1");
    expected.add("toto2");
    expected.add("toto1");
    expected.add("5");
    expected.add("toto1toto2");
    expected.add("7");

    List<String> result = service.fizzBuzz(2, 3, 7, "toto1", "toto2");

    assertThat(result.size()).isEqualTo(expected.size());
    for (int ii = 0; ii < expected.size(); ii++) {
      assertThat(result.get(ii)).isEqualTo(expected.get(ii));
    }
  }

  @Test
  public void fizzbuzzWrongLimitTest() {

    assertThrows(FizzBuzzException.class, () -> service.fizzBuzz(2, 3, -2, "toto", "tata"));
  }

  @Test
  public void fizzbuzzWrongNb1Test() {

    assertThrows(FizzBuzzException.class, () -> service.fizzBuzz(-10, 3, 10, "toto", "tata"));
  }

  @Test
  public void fizzbuzzWrongNb2Test() {

    assertThrows(FizzBuzzException.class, () -> service.fizzBuzz(2, -10, 10, "toto", "tata"));
  }
}
