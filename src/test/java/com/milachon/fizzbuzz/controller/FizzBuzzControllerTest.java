package com.milachon.fizzbuzz.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.milachon.fizzbuzz.domain.model.FizzBuzzRequest;
import com.milachon.fizzbuzz.manager.FizzBuzzRequestManager;
import com.milachon.fizzbuzz.service.FizzBuzzService;

@WebMvcTest(FizzBuzzRestController.class)
public class FizzBuzzControllerTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private FizzBuzzService service;

  @MockBean private FizzBuzzRequestManager manager;

  @Test
  public void computeTest() throws Exception {

    List<String> res = new ArrayList<String>();
    res.add("resultat1");

    when(service.fizzBuzz(1, 3, 10, "toto", "tata")).thenReturn(res);
    this.mockMvc
        .perform(
            get("/fizzbuzz/compute")
                .param("int1", "1")
                .param("int2", "3")
                .param("limit", "10")
                .param("str1", "toto")
                .param("str2", "tata"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("result")));
  }

  @Test
  public void mostCalledTest() throws Exception {

    FizzBuzzRequest req = new FizzBuzzRequest();
    req.setCounter(3);

    when(manager.getMostCalledRequest()).thenReturn(req);
    this.mockMvc
        .perform(get("/fizzbuzz/most_called"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("\"counter\":3")));
  }

  @Test
  public void mostCalledNonExistTest() throws Exception {

    when(manager.getMostCalledRequest()).thenReturn(null);
    this.mockMvc
        .perform(get("/fizzbuzz/most_called"))
        .andExpect(status().is(HttpStatus.SC_NO_CONTENT));
  }
}
