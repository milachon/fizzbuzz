# fizzbuzz

## Prerequisites

* Need Java >8 
* Gradle >6.3 or use provided wrapper

## Build

* execute command "gradle bootjar"
* jar is available in /build/libs/

* jar is also available (limited time) in artifact of CI job

## Launch
Execute command: java -jar fizzbuzz-0.1.0.jar [--server.port=8080]

## Exposed API 
Auto generated api doc: 
http://localhost:8080/swagger-ui.html


## Admin: Config 
Configuration (like DB connection) are in src/main/resources/application.yml

## Comment: 
Postula for exercice: it works with only positiv non zero integer to avoid divide by zero

## Exercise definition 
Write a simple fizz-buzz REST server. 

The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz", and all multiples of 15 by "fizzbuzz". The output would look like this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".

Your goal is to implement a web server that will expose a REST API endpoint that: 

Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.

Returns a list of strings with numbers from 1 to limit, where: all multiples of int1 are replaced by str1, all multiples of int2 are replaced by str2, all multiples of int1 and int2 are replaced by str1str2.
